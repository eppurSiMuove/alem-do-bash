#!/usr/bin/env bash
# -----------------------------------------------------
# Script		: codenow
# Descrição		: Escreve automaticamente campos iniciais e cabeçalho
#				: e dá permissão de execução a um novo arquivo de código-fonte.
# Versão		: 0.5
# Autor			: Eppur Si Muove
# Contato		: eppur.si.muove.89@keemail.me
# Criação		: 04/07/2020
# Modificação	: 26/07/2020
# Licença		: GNU/GPL v3.0
# -----------------------------------------------------

declare -A MODELO['sh']='#!/usr/bin/env bash
# =INFO=

-- CDNEditor : vi'
MODELO['php']='#!/usr/bin/env php
<?php
// =INFO=

?>
-- CDNEditor : vi'
MODELO['c']='// =INFO=
#include <stdio.h>
#include <stdlib.h>

int main () {

	return 0;
}

-- CDNEditor : vi'
MSG[0]='Uso:

$ codenow [[help]|[list]|[config]] | [filename TIPO]

help
	: mostra esse menu e sai.

list
	: lista os arquivos executáveis no diretório atual.

config
	: configura editor padrão e informações constantes nos
	: cabeçalhos dos arquivos que serão criados pelo CODENOW.

filename
	: Esse argumento, assim como os anteriores, não é obrigatório, mas caso
	: um nome de arquivo seja informado, torna-se obrigatório informar o TIPO,
	: pois esta define o conteúdo padrão do código a ser inserido.

TIPO
	: Inicialmente, somente os tipos SH, PHP e C estão disponíveis,
	: mas o suporte a outras linguagens pode ser extendido dentro do diretório
	: de configuração através dos arquivos modelo. Cada TIPO tem o seu próprio
	: arquivo, exemplo: modelo.sh, modelo.php (Ver seção DEFINIÇÕES DE TIPOS).

~/.config/codenow/config
	: arquivo de configuração para definição de valores padrão para o editor
	: favorito e os campos de cabeçalho Autor, Contato e Licença. Também é
	: possível definir os tipos de arquivos que esse script pode criar, para
	: isso, por favor leia este arquivo de configuração que é criado
	: automaticamente na primeira vez que esse script é executado.
		
DEFINIÇÕES DE TIPOS
	Para definir novo tipo de arquivo de código que o CODENOW poderá criar,
	é necessário que um modelo com o nome "modelo.TIPO" seja criado dentro
	do diretório de configuração ("TIPO" é a abreviação da linguagem desejada),
	em $HOME/.config/codenow , seguindo os seguintes padrões:

			[CÓDIGO]
			CMT =INFO=
			[CÓDIGO]
				.
				.
				.
			[CÓDIGO]
			-- CDNEditor : EDITOR

	Onde,

	CÓDIGO
		todo o código necessário para compor o arquivo do TIPO especificado.

	CMT
		caractere ou conjunto de caracteres que identificam o ínicio de uma
		linha única de comentário. Para HTML e CSS indicar, html e css, respectivamente.

	=INFO=
		coloque esse marcador na parte do código que queira inserir os dados do programa
		como nome do arquivo, autor, contato, licença, data.

	CDNEditor : EDITOR
		indica o editor padrão para esse TIPO. Deve ser especficado logo após o "--" .

	--
		marcador para o CODENOW saber qual é o final do código para o tipo especificado.'
MSG[1]='Erro: %s : o CODENOW não pode criar o diretório de configuração.'
MSG[2]='Executáveis (em %s)\n'
MSG[3]='Deseja criar um novo código? (s/n): '
MSG[4]='Erro: %s : não foi possível alterar seu arquivo de configuração.'
MSG[5]='Erro: %s : modelo não existe para esse TIPO.'
MSG[6]='Erro: %s : contém zero ou mais de um campo =INFO= para esse TIPO.'
MSG[7]='Erro: %s : campo CMT está vazio para esse TIPO.'
MSG[8]='Erro: %s : contém zero ou mais de um campo -- para esse TIPO.'
MSG[9]='Erro: %s : editor inválido ou não especifiado para esse arquivo.'
MSG[10]='Nome para o novo código: '
MSG[11]='Erro: %s : help, list e config são palavras reservadas do CODENOW.'
MSG[12]='Erro: %s : já existe um arquivo com esse nome.'
MSG[13]='Escolher outro (N)ome, abrir para (e)ditar; ou (s)air? :  '
MSG[14]='Erro: %s : você não tem permissão de escrita nesse diretório.'
MSG[15]='Erro: %s : não foi possível criar arquivo.'
MSG[16]='Erro: %s : não foi possível conceder permissão de execução do arquivo para seu usuário.'
MSG[17]='Deseja abrir o documento? (s/n): '
MSG[18]='Erro: %d : quantidade de argumentos inválida.'
MSG[19]='Tipo de código (sh, php, c ...) : '
INFO='-----------------------------------------------------
Nome		: %s
Descrição	: << descrição breve de sua funcionalidade >>
Versão		: 0.1-beta
Autor		: %s
Contato		: %s
Criação		: %s
Modificação	:
Licença		: %s
CDNEditor	: %s
-----------------------------------------------------'
CRIACAO=$(date +'%d/%m/%Y')
CONFIG="$HOME/.config/codenow/config"
DIRETORIO="$(pwd)"
EDITOR=''
ARQUIVO=''
CONTEUDO=''
COMENTARIO=''
declare -A ARRCFG=(	[Autor]="$(whoami)" \
					[Contato]="$(whoami)@$(hostname -f)" \
					[Licença]='GNU/GPL v3.0' )
# FUNÇÕES |-------------------------------------------------------
_mostraMsg(){
	printf "${MSG[$1]}\n" "$2"
}

_carregaConfigs(){ 
	local tmpCfg
	local cfgDir="${CONFIG%/*}"
	local cfgMod="${cfgDir}/modelo"

	# sai da função com estado de erro, caso não seja possível criar diretório de configuração
	if ! mkdir -p "$cfgDir"; then _mostraMsg 1 "$cfgDir"; return 1; fi

	# caso arquivo de configuração caso não exista, cria um novo, senão carrega os seus valores
	if [[ ! -f $CONFIG ]]; then
		for cfg in ${!ARRCFG[@]}; do
			echo "$cfg: ${ARRCFG[$cfg]}" >> "$CONFIG"
		done
	else
		for cfg in ${!ARRCFG[@]}; do
			tmpCfg=$(sed -n -e "s/^$cfg: //p" "$CONFIG")
			ARRCFG[$cfg]="$tmpCfg"
		done
	fi

	# verifica se os modelos padrões (sh, php e c) existem no diretório de configuração
	for ext in ${!MODELO[@]};do
		[[ ! -f $cfgMod'.'$ext ]] && echo "${MODELO[$ext]}" >> $cfgMod'.'$ext
	done

	return 0
}

_listaExec(){ 
	local dir=$(pwd)
	local continuar
	
	_mostraMsg 2 "$dir"
	for arq in "$dir"/*; do
		[[ -x "$arq" && -f "$arq" ]] && echo "${arq##*/}"
	done
	echo

	read -n 1 -p "${MSG[3]}" continuar
	echo
	[[ "${continuar,,}" == 's' ]] && return 1 || return 0
}

_editaConfig(){
	local continuar
	local tmpCfg
	local cfg

	# solicita ao usuário entrar com os novos valores de configuração
	for cfg in ${!ARRCFG[@]}; do
		while [[ $tmpCfg =~ [^[:alnum:]"@-._"] || $tmpCfg == "" ]]; do
			read -p "Novo valor - $cfg: " tmpCfg
			[[ "$tmpCfg" == *"/"* ]] && tmpCfg=${tmpCfg//\//\\/}
		done
		# sai da função com erro, caso sed não consiga alterar o arquivo de configuração
		if ! (sed -i -e "s/$cfg: .*$/$cfg: $tmpCfg/g" "$CONFIG"); then
			_mostraMsg 4 "$CONFIG"
			return 4
		fi
		tmpCfg=''
	done

	read -n 1 -p "${MSG[3]}" continuar
	echo
	[[ "${continuar,,}" == 's' ]] && return 1 || return 0
}

_checaEditor(){
	EDITOR=$(sed -n 's/.*CDNEditor*[[:space:]]*: \(.*\)/\1/p' "$1")
	while :; do
		if ! which $EDITOR &> /dev/null; then 
			_mostraMsg 9 "$1"
			read -n1 -p "Tecle: [i]ndicar um editor válido ou [S]air: "
			[[ "${REPLY,,}" == 'i' ]] && { echo; read -p "Editor: " EDITOR; } || { echo; exit 0; }
		else
			return 0
		fi
	done
}

_checaModelo(){
	local modelo
	local ext="$1"
	
	# verifica se usuário passou o TIPO como argumento
	while :; do
		if [[ -z $ext ]]; then
			read -p "${MSG[19]}" ext
			continue
		else
			break
		fi
	done

	modelo=${CONFIG%/*}/modelo.$ext

	# verifica se o modelo para para o TIPO $ext existe
	[[ ! -f "$modelo" ]] && _mostraMsg 5 $ext && return 5

	# contém apenas um campo =INFO= ?
	[[ $(grep -o -c '=INFO=' "$modelo") -ne 1 ]] && _mostraMsg 6 $ext && return 6

	# contém o campo CMT ? (indica comentário)
	COMENTARIO=$(sed -n -e 's/ =INFO=//p' $modelo)
	[[ -z "$COMENTARIO" ]] && _mostraMsg 7 $ext && return 7

	# contém apenas um campo '--'? (indica final do arquivo)
	[[ $(grep -o -c '\-\-' "$modelo") -ne 1 ]] && _mostraMsg 8 $ext && return 8

	# editor válido foi especificado ?
	_checaEditor "$modelo"
	
	if ! _formataConteudo $ext; then return $?; fi
	return 0
}

_formataConteudo(){
	# formata o simbolo que indica comentário
	[[ "$COMENTARIO" == '%' ]] && COMENTARIO='\%'
	[[ "$COMENTARIO" == // ]] && COMENTARIO='\/\/'
	if [[ "$COMENTARIO" != 'html' && "$COMETARIO" != 'css' ]]; then
		INFO=$(echo "$INFO" | sed -e "s/^/$COMENTARIO /g")
	else
		# tratamento de html e css
		local cInicio
		local cFinal
		[[ "$COMENTARIO" == "html" ]] && { cInicio='<\!--'; cFinal='-->'; }
		[[ "$COMENTARIO" ==  "css" ]] && { cInicio='\/*'; cFinal='*\/'; }
		INFO=$(echo "$INFO" | sed -e "s/^/\t/g; 1 i $cInicio" | sed -e "$ a $cFinal")
	fi

	# preenche o cabeçalho com as informações de nome, autor, contato, criação e licença
	INFO=$(printf "$INFO" "${ARQUIVO##*/}" "${ARRCFG[Autor]}" "${ARRCFG[Contato]}" "$CRIACAO" "${ARRCFG[Licença]}" $EDITOR)

	# preenche o conteúdo com o código, as informações comentadas e o editor
	CONTEUDO=$(cat "${CONFIG%/*}/modelo.$1" | sed '$ d')
	CONTEUDO=$(echo "$CONTEUDO" | sed -e "s/$COMENTARIO =INFO=/%s/g")
	CONTEUDO=$(printf "$CONTEUDO" "$INFO")
	return 0
}

_defineNome(){
	local nome="$1"
	local arq="$DIRETORIO/$nome"

	while :; do
		# caso nome seja inválido, fica preso no if
		if [[ $nome =~ [^[:alnum:]"._-"] || $nome == *" "* || $nome == "" ]]; then
			read -p "${MSG[10]}" nome
			arq="$DIRETORIO/$nome"
			continue
		fi

		# nome não pode ser help, list, config
		[[ $nome == 'help' || $nome == 'list' || $nome == 'config' ]] && { _mostraMsg 11 $nome; nome=''; continue; }

		# verifica se já existe um arquivo com esse nome
		if [[ -e "$arq" ]]; then
			_mostraMsg 12 "$nome"
			if [[ -f "$arq" ]]; then
				# se for um arquivo, pergunta se quer passar novo nome, editar ou sair.
				read -n 1 -p "${MSG[13]}"
				case ${REPLY,,} in
					'e') if _checaEditor "$nome"; then _abreArquivo "$nome"; else return 9; fi; continue ;;
					's') echo; exit 0 ;;
				esac
			fi
			echo
			nome=''
			continue
		fi

		break
	done

	# se tudo der certo o nome para ARQUIVO é definido
	ARQUIVO="$arq"
	return
}

_criarArquivo(){
	# verifica se usuário tem acesso à escrita no diretório
	[[ ! -w "$DIRETORIO" ]] && _mostraMsg 60 "$DIRETORIO" && return 14

	# cria o arquivo
	if ! echo "$CONTEUDO" >> "$ARQUIVO"; then _mostraMsg 15; return 15; fi

	# concede permissão de execução para dono e grupo dono
	if ! chmod ug+x "$ARQUIVO"; then _mostraMsg 16; return 16; fi

	# pergunta se quer abrir e editar o arquivo
	read -n 1 -p "${MSG[17]}"
	[[ $REPLY == 's' ]] && return 17 || return 0
}

_abreArquivo(){
	$EDITOR "$1"
	return 0
}

_principal(){
	if ! _carregaConfigs; then _mostraMsg 0; exit 1; fi

	if ! _defineNome "$1"; then exit 1; fi

	if ! _checaModelo "$2"; then exit 1; fi

	_criarArquivo
	[[ $? -eq 17 ]] && _abreArquivo "$ARQUIVO" || return 1

	return 0
}

# INÍCIO DO PROGRAMA | -----------------------------------------------
if [[ $# -eq 1 ]]; then
	case "$1" in
		'help') less <<< "${MSG[0]}";;
		'list') if ! _listaExec; then _principal; fi ;;
		'config') if ! _editaConfig; then _principal; fi ;;
		*) _principal "$1" ;;
	esac
elif [[ $# -eq 0 || $# -eq 2 ]]; then
	_principal "$1" "$2"
else
	_mostraMsg 18 $#
	exit 18
fi

exit $?

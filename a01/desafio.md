# 1 - Script Gerador de Scripts

Crie um programa para gerar um modelo para um novo script na pasta em que você estiver trabalhando.

## Requisitos do projeto

1. O conteúdo automático do script gerado deve ser a *shebang*: `#!/usr/bin/env bash\n\n`;
2. O arquivo gerado deve ser tronado executável;
3. O nome do novo script deve ser passado como primeiro parâmetro posicional da chamada do seu programa;
4. Verificar se o nome informado já existe na pasta de destino;
5. Verificar se é um nome válido (sem espaços);
6. Verificar se foi passado mais de um parâmetro para o seu programa;
7. Se o nome existir, outro nome deve ser solicitado;
8. Se o nome for inválido, solicitar outro nome;
9. Ao solicitar outro nome, repetir as verificações anteriores e continuar pedindo um nome válido até o usuário cancelar ou realmente informar um nome válido;
10. Se houver mais de um parâmetro, apenas exibir ajuda e sair;
11. O programa deve oferecer uma opção `l` (L minúsculo) e `list` para listar os executáveis da pasta corrente e `h' e `help` para exibir a ajuda e sair;
12. Nomes `l`, `h`, `list` e `help` são nomes inválidos;
13. Após a opção `list`, o usuário deve ser questionado se deseja informar um nome de arquivo ou sair;
14. Após criado o novo arquivo, o programa deve perguntar se o usuário deseja abrir o arquivo para edição com o editor padrão dele;
15. O editor padrão deve ser informado em um arquivo de configuração em `~/.config/nome_do_script_gerador/config`;
16. O arquivo de configuração no local acima deve ser criado ou editado (se existir) com a opção `config`;
17. O nome `config` também será inválido;
18. O conteúdo do arquivo de configuração deverá ser apenas o comando para abrir o editor a ser utilizado;
19. O editor deve ser aberto com o utilitário `setsid` (não informado na configuração);

## Requisitos de estilo

* Todas as funções devem ter nomes minúsculos iniciados com sublinhado (`_`);
* Todas as variáveis devem ter nomes minúsculos;
* Variáveis usadas apenas nas funções devem ser locais (palavra-chave `local`);
* Variáveis globais devem ser definidas antes de tudo no código;
* Funções devem ser definidas após as variáveis;
* As rotinas principais devem aparecer após as funções.

## Comandos e recursos programáticos envolvidos

* Comando `echo`
* Comando `read`
* O comando composto `[[ EXPRESSÃO ]]`
* O comando composto `if`, `elif`, `else`
* O comando composto `case`
* O comando composto `while` ou `until`
* O comando composto `for`
* Criação e expansão de variáveis
* Funções
* Parâmetros posicionais
* Expansões condicionais
* Expansões de caixa de texto
* O redirecionamento de *append* `>>`
* O utilitário `chmod`
* O utilitário `mkdir`
* O utilitário `cat`
* O utilitário `setsid`

## Desafio extra!

Adapte seu programa incluindo mais opções para a criação de outros modelos de arquivos, por exemplo...

### Códigos fonte da linguagem C (não executável)

```
#include <stdio.h>
#include <stdlib.h>

int main() {



    return 0;
}
```

### Scripts executáveis em PHP-CLI

```
#!/usr/bin/env php
<?php

```

> **Dica!** Talvez seja interessante criar arquivos de modelos na pasta de configuração. Assim o seu código fica menor e o usuário pode modificar os modelos conforme a necessidade.
